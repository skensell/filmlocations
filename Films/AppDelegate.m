//
//  AppDelegate.m
//  Films
//
//  Created by Scott Kensell on 7/2/19.
//  Copyright © 2019 Scott Kensell. All rights reserved.
//

#import "AppDelegate.h"

#import "MXFilmLocationsViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:[[MXFilmLocationsViewController alloc] init]];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}


@end
