//
//  MXFilmLocationViewController.m
//  Films
//
//  Created by Scott Kensell on 7/2/19.
//  Copyright © 2019 Scott Kensell. All rights reserved.
//

#import "MXFilmLocationViewController.h"

#import "MXFilmLocation.h"
#import "MXFilmLocationView.h"
#import "MXNetworkSession.h"
#import "UIView+MXAdditions.h"

@interface MXFilmLocationViewController ()

// UI
@property (nonatomic, strong) UIScrollView* scrollView;
@property (nonatomic, strong) MXFilmLocationView* filmLocationView;

// Dependencies
@property (nonatomic, strong) MXNetworkSession* networkSession;

@end

@implementation MXFilmLocationViewController

- (instancetype)initWithFilmLocation:(MXFilmLocation *)filmLocation networkSession:(MXNetworkSession *)networkSession {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _filmLocation = filmLocation;
        _networkSession = networkSession;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = self.filmLocation.locationName ? : @"Film Location";
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIScrollView* scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    self.scrollView = scrollView;
    [self.view addSubview:scrollView];
    [self.view mx_addConstraintsForSpanningChildView:scrollView];
    
    MXFilmLocationView* filmLocationView = [[MXFilmLocationView alloc] initWithFilmLocation:self.filmLocation];
    self.filmLocationView = filmLocationView;
    [self.scrollView addSubview:filmLocationView];
    [self.scrollView mx_addConstraintsForSpanningChildView:filmLocationView];
    [self.scrollView.widthAnchor constraintEqualToAnchor:filmLocationView.widthAnchor].active = YES;
}

@end
