//
//  MXImageDownloader.m
//  Films
//
//  Created by Scott Kensell on 7/2/19.
//  Copyright © 2019 Scott Kensell. All rights reserved.
//

#import "MXImageDownloader.h"

@interface MXImageDownloader()

@property (nonatomic, strong) NSURLSession* session;

@end

@implementation MXImageDownloader

- (instancetype)init {
    self = [super init];
    if (self) {
        // This is just a cheap way to do some simple caching. Larger projects needing a more configurable,
        // two-level image cache might consider something like SDWebImage.
        NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
        config.requestCachePolicy = NSURLRequestReturnCacheDataElseLoad;
        config.HTTPMaximumConnectionsPerHost = 10;
        // Normally you wouldn't want the delegateQueue to be the main queue, but since we are not specifying
        // a delegate, the main thread shouldn't suffer.
        _session = [NSURLSession sessionWithConfiguration:config delegate:nil delegateQueue:[NSOperationQueue mainQueue]];
    }
    return self;
}

- (void)downloadImageAtURL:(NSURL *)imageURL completion:(MXImageDownloaderCompletionBlock)completion {
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:imageURL];
    NSURLSessionDataTask* task = [self.session dataTaskWithRequest:request completionHandler:completion];
    [task resume];
}

- (void)invalidateAndCancel {
    [_session invalidateAndCancel];
    _session = nil;
}

@end
