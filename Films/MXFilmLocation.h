//
//  MXFilmLocation.h
//  Films
//
//  Created by Scott Kensell on 7/2/19.
//  Copyright © 2019 Scott Kensell. All rights reserved.
//

#import "MXModel.h"

@protocol MKAnnotation;

@interface MXFilmLocation : MXModel

// core properties
@property (nonatomic, strong) NSArray<NSString*>* actors;
@property (nonatomic, strong) NSString* director;
@property (nonatomic, strong) NSString* funFacts;
@property (nonatomic, strong) NSNumber* latitude;
@property (nonatomic, strong) NSNumber* longitude;
@property (nonatomic, strong) NSString* locationName;
@property (nonatomic, strong) NSString* releaseYear;
@property (nonatomic, strong) NSString* productionCompany;
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSArray<NSString*>* writers;

// derived properties and methods
@property (nonatomic, strong) NSURL* omdbAPIURL;
@property (nonatomic, strong) id<MKAnnotation> annotation;

@end
