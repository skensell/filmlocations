//
//  MXFilmLocationView.h
//  Films
//
//  Created by Scott Kensell on 7/2/19.
//  Copyright © 2019 Scott Kensell. All rights reserved.
//

@class MXFilmLocation;

@interface MXFilmLocationView : UIView

@property (nonatomic, strong, readonly) MXFilmLocation* filmLocation;

- (instancetype)initWithFilmLocation:(MXFilmLocation*)filmLocation;

@end
