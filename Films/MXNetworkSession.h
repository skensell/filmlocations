//
//  MXNetworkSession.h
//  Films
//
//  Created by Scott Kensell on 2/25/18.
//  Copyright © 2018 Scott Kensell. All rights reserved.
//

NS_ASSUME_NONNULL_BEGIN

typedef void(^MXNetworkSessionDataTaskCompletionBlock)(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error);

/// All completion blocks called on main thread.
@interface MXNetworkSession : NSObject

+ (instancetype)instance;

- (void)fetchFilmLocationsCompletion:(MXNetworkSessionDataTaskCompletionBlock)completion;
- (void)fetchArbitraryJSONData:(NSURL*)url completion:(MXNetworkSessionDataTaskCompletionBlock)completion;

@end

NS_ASSUME_NONNULL_END
