//
//  UIView+MXAdditions.h
//  Films
//
//  Created by Scott Kensell on 2/25/18.
//  Copyright © 2018 Scott Kensell. All rights reserved.
//

@interface UIView (MXAdditions)

- (void)mx_addConstraintsForSpanningChildView:(UIView*)child;

@end
