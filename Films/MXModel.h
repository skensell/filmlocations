//
//  MXModel.h
//  Films
//
//  Created by Scott Kensell on 2/26/18.
//  Copyright © 2018 Scott Kensell. All rights reserved.
//

// NOTE: This is a poor man's version of Mantle's MTLModel.
@interface MXModel : NSObject

- (instancetype)initWithDictionary:(NSDictionary*)dic error:(NSError* __autoreleasing *)error NS_DESIGNATED_INITIALIZER;

+ (NSDictionary*)JSONKeyPathsByPropertyKey; // abstract
+ (NSArray*)fromArray:(NSArray<NSDictionary*>*)jsonArray error:(NSError* __autoreleasing *)error;


@end
