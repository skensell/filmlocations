//
//  MXFilmLocationView.m
//  Films
//
//  Created by Scott Kensell on 7/2/19.
//  Copyright © 2019 Scott Kensell. All rights reserved.
//

#import "MXFilmLocationView.h"

#import "MXFilmLocation.h"
#import "MXImageDownloader.h"
#import "MXNetworkSession.h"

#import <MapKit/MapKit.h>

@interface MXFilmLocationView()

@property (nonatomic, strong, readonly) MXImageDownloader* imageDownloader;
@property (nonatomic, strong, readonly) MXNetworkSession* networkSession;

@property (nonatomic, strong) NSLayoutConstraint* posterImageHeightConstraint;
@property (nonatomic, strong) NSLayoutConstraint* posterImageSpaceAfterConstraint;

@end

@implementation MXFilmLocationView {
    MKMapView* _mapView;
    UIImageView* _posterImageView;
    UILabel* _locationNameLabel;
    NSArray <NSArray*>* _tableLabels;
}

- (instancetype)initWithFilmLocation:(MXFilmLocation *)filmLocation {
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        _filmLocation = filmLocation;
        _imageDownloader = [[MXImageDownloader alloc] init];
        _networkSession = [MXNetworkSession instance];
        
        _mapView = [[MKMapView alloc] initWithFrame:CGRectZero];
        _mapView.translatesAutoresizingMaskIntoConstraints = NO;
        if (_filmLocation.annotation) {
            [_mapView addAnnotation:_filmLocation.annotation];
            [_mapView showAnnotations:@[_filmLocation.annotation] animated:NO];
        }
        [self addSubview:_mapView];
        
        if (_filmLocation.omdbAPIURL) {
            _posterImageView = [[UIImageView alloc] init];
            _posterImageView.translatesAutoresizingMaskIntoConstraints = NO;
            _posterImageView.contentMode = UIViewContentModeScaleAspectFill;
            [self addSubview:_posterImageView];
            [self downloadPosterImage];
        }

        _locationNameLabel = [self makeFlexibleLabel];
        _locationNameLabel.attributedText = [[NSAttributedString alloc] initWithString:(filmLocation.locationName ? : @"") attributes:@{NSFontAttributeName: [UIFont fontWithName:@"AvenirNext-Bold" size:24.0f]}];
        [self addSubview:_locationNameLabel];
        
        [self createTableOfFilmLocationDetails];

        [self setupConstraints];
    }
    return self;
}

- (void)dealloc {
    [_imageDownloader invalidateAndCancel];
    _imageDownloader = nil;
}

- (UILabel*)makeFlexibleLabel {
    UILabel* label = [[UILabel alloc] init];
    label.translatesAutoresizingMaskIntoConstraints = NO;
    label.numberOfLines = 0;
    return label;
}

- (UILabel*)makeTableRowTitleLabel:(NSString*)title {
    UILabel* label = [self makeFlexibleLabel];
    label.attributedText = [[NSAttributedString alloc] initWithString:title attributes:@{NSFontAttributeName: [UIFont fontWithName:@"Avenir-Heavy" size:15.0f]}];
    return label;
}

- (UILabel*)makeTableRowValueLabel:(NSString*)text {
    UILabel* label = [self makeFlexibleLabel];
    label.attributedText = [[NSAttributedString alloc] initWithString:text attributes:@{NSFontAttributeName: [UIFont fontWithName:@"AvenirNext-Medium" size:15.0f]}];
    return label;
}

- (void)createTableOfFilmLocationDetails {
    NSMutableArray* tableStrings = [[NSMutableArray alloc] init];
    if (_filmLocation.title.length > 0) {
        [tableStrings addObject:@[@"Title", _filmLocation.title]];
    }
    if (_filmLocation.releaseYear.length > 0) {
        [tableStrings addObject:@[@"Released", _filmLocation.releaseYear]];
    }
    if (_filmLocation.actors.count > 0) {
        [tableStrings addObject:@[@"Featuring", [_filmLocation.actors componentsJoinedByString:@", "]]];
    }
    if (_filmLocation.director.length > 0) {
        [tableStrings addObject:@[@"Directed by", _filmLocation.director]];
    }
    if (_filmLocation.writers.count > 0) {
        [tableStrings addObject:@[@"Written by", [_filmLocation.writers componentsJoinedByString:@", "]]];
    }
    if (_filmLocation.productionCompany.length > 0) {
        [tableStrings addObject:@[@"Produced by", _filmLocation.productionCompany]];
    }
    if (_filmLocation.funFacts.length > 0) {
        [tableStrings addObject:@[@"Fun Fact", _filmLocation.funFacts]];
    }
    
    NSMutableArray* tableLabels = [[NSMutableArray alloc] initWithCapacity:tableStrings.count];
    [tableStrings enumerateObjectsUsingBlock:^(NSArray* titleAndValue, NSUInteger idx, BOOL *stop) {
        UILabel* titleLabel = [self makeTableRowTitleLabel:titleAndValue[0]];
        UILabel* valueLabel = [self makeTableRowValueLabel:titleAndValue[1]];
        [self addSubview:titleLabel];
        [self addSubview:valueLabel];
        [tableLabels addObject:@[titleLabel, valueLabel]];
    }];
    _tableLabels = tableLabels;
}

- (void)setupConstraints {
    [NSLayoutConstraint activateConstraints:@[
        [_mapView.leftAnchor constraintEqualToAnchor:self.leftAnchor],
        [_mapView.topAnchor constraintEqualToAnchor:self.topAnchor],
        [_mapView.rightAnchor constraintEqualToAnchor:self.rightAnchor],
        [_mapView.heightAnchor constraintEqualToAnchor:_mapView.widthAnchor multiplier:0.75f]
    ]];
    
    UILayoutGuide* textMargins = [[UILayoutGuide alloc] init];
    [self addLayoutGuide:textMargins];
    [NSLayoutConstraint activateConstraints:@[
        [textMargins.leftAnchor constraintEqualToAnchor:self.leftAnchor constant:16.0f],
        [textMargins.rightAnchor constraintEqualToAnchor:self.rightAnchor constant:-16.0f],
        [textMargins.topAnchor constraintEqualToAnchor:_mapView.bottomAnchor constant:16.0f],
        [textMargins.bottomAnchor constraintLessThanOrEqualToAnchor:self.bottomAnchor constant:-16.0f],
    ]];
    
    NSLayoutYAxisAnchor* currentYAnchor = textMargins.topAnchor;
    
    if (_posterImageView) {
        self.posterImageHeightConstraint = [_posterImageView.heightAnchor constraintEqualToConstant:0.0f];
        [NSLayoutConstraint activateConstraints:@[
            [_posterImageView.leftAnchor constraintEqualToAnchor:textMargins.leftAnchor],
            [_posterImageView.topAnchor constraintEqualToAnchor:currentYAnchor],
            [_posterImageView.widthAnchor constraintEqualToConstant:90.0f],
            self.posterImageHeightConstraint
        ]];
        UILayoutGuide* spacer = [[UILayoutGuide alloc] init];
        [self addLayoutGuide:spacer];
        self.posterImageSpaceAfterConstraint = [spacer.topAnchor constraintEqualToAnchor:_posterImageView.bottomAnchor constant:0];
        self.posterImageSpaceAfterConstraint.active = YES;
        currentYAnchor = spacer.topAnchor;
    }
    
    [NSLayoutConstraint activateConstraints:@[
        [_locationNameLabel.leftAnchor constraintEqualToAnchor:textMargins.leftAnchor],
        [_locationNameLabel.rightAnchor constraintLessThanOrEqualToAnchor:textMargins.rightAnchor],
        [_locationNameLabel.topAnchor constraintEqualToAnchor:currentYAnchor]
    ]];
    currentYAnchor = _locationNameLabel.bottomAnchor;
    
    UILayoutGuide* spacerBelowNameLabels = [[UILayoutGuide alloc] init];
    [self addLayoutGuide:spacerBelowNameLabels];
    [spacerBelowNameLabels.topAnchor constraintEqualToAnchor:currentYAnchor constant:10.0f].active = YES;
    currentYAnchor = spacerBelowNameLabels.topAnchor;
    
    for (NSArray* labels in _tableLabels) {
        UILabel* leftLabel = labels[0];
        UILabel* rightLabel = labels[1];
        [NSLayoutConstraint activateConstraints:@[
            [leftLabel.leftAnchor constraintEqualToAnchor:textMargins.leftAnchor],
            [leftLabel.topAnchor constraintEqualToAnchor:currentYAnchor constant:8.0f],
            [leftLabel.rightAnchor constraintEqualToAnchor:textMargins.centerXAnchor],
            [rightLabel.leftAnchor constraintEqualToAnchor:leftLabel.rightAnchor],
            [rightLabel.topAnchor constraintEqualToAnchor:leftLabel.topAnchor],
            [rightLabel.rightAnchor constraintEqualToAnchor:textMargins.rightAnchor],
        ]];
        
        UILayoutGuide* spacerToGetMaxBottom = [[UILayoutGuide alloc] init];
        [self addLayoutGuide:spacerToGetMaxBottom];
        [spacerToGetMaxBottom.topAnchor constraintGreaterThanOrEqualToAnchor:leftLabel.bottomAnchor].active = YES;
        [spacerToGetMaxBottom.topAnchor constraintGreaterThanOrEqualToAnchor:rightLabel.bottomAnchor].active = YES;
        currentYAnchor = spacerToGetMaxBottom.bottomAnchor;
    }
    
    [textMargins.bottomAnchor constraintEqualToAnchor:currentYAnchor].active = YES;
}

- (void)downloadPosterImage {
    if (_filmLocation.omdbAPIURL == nil) {
        return;
    }
    
    __weak UIImageView* weakImageView = _posterImageView;
    __weak MXFilmLocationView* weakSelf = self;
    
    [_networkSession fetchArbitraryJSONData:_filmLocation.omdbAPIURL completion:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        __strong typeof(self) self = weakSelf;
        if (!data) {
            return; // fail silently, this isn't critical data
        }
        NSDictionary* omdbFilmObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
        if (!omdbFilmObject || ![omdbFilmObject isKindOfClass:[NSDictionary class]]) {
            return; // fail silently, this isn't critical data
        }
        
        NSString* posterImageString = omdbFilmObject[@"Poster"];
        NSURL* posterImageURL = [NSURL URLWithString:posterImageString];
        if (!posterImageURL) {
            return; // fail silently, this isn't critical data
        }
        
        [self.imageDownloader downloadImageAtURL:posterImageURL completion:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            if (data) {
                UIImage* image = [UIImage imageWithData:data];
                if (image) {
                    weakImageView.image = image;
                    weakSelf.posterImageHeightConstraint.constant = 160.0f;
                    weakSelf.posterImageSpaceAfterConstraint.constant = 10.0f;
                }
            }
        }];
    }];
}

@end
