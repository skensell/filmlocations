//
//  MXFilmLocation.m
//  Films
//
//  Created by Scott Kensell on 7/2/19.
//  Copyright © 2019 Scott Kensell. All rights reserved.
//

#import "MXFilmLocation.h"

#import "MXFilmLocationAnnotation.h"

@implementation MXFilmLocation

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"actors": @"actors",
             @"director": @"director",
             @"funFacts": @"fun_facts",
             @"latitude": @"latitude",
             @"longitude": @"longitude",
             @"locationName": @"locations",
             @"releaseYear": @"release_year",
             @"productionCompany": @"production_company",
             @"title": @"title",
             @"writers": @"writers"
             };
}

- (instancetype)initWithDictionary:(NSDictionary *)dic error:(NSError *__autoreleasing *)error {
    self = [super initWithDictionary:dic error:error];
    if (self) {
        if (_title.length > 0) {
            // Set the derived property _omdbAPIURL
            NSURLComponents* omdbURLComponents = [[NSURLComponents alloc] initWithString:@"http://www.omdbapi.com/"];
            NSURLQueryItem* apiKeyQueryParameter = [NSURLQueryItem queryItemWithName:@"apikey" value:@"6ff8ac9e"];
            NSURLQueryItem* titleQueryParameter = [NSURLQueryItem queryItemWithName:@"t" value:_title];
            omdbURLComponents.queryItems = @[apiKeyQueryParameter, titleQueryParameter,];
            _omdbAPIURL = omdbURLComponents.URL;
        }
        
        MXFilmLocationAnnotation* annotation = [[MXFilmLocationAnnotation alloc] init];
        annotation.coordinate = CLLocationCoordinate2DMake([_latitude doubleValue], [_longitude doubleValue]);
        annotation.title = _locationName;
        annotation.subtitle = _title;
        _annotation = annotation;
    }
    return self;
}

@end
