//
//  MXModel.m
//  Films
//
//  Created by Scott Kensell on 2/26/18.
//  Copyright © 2018 Scott Kensell. All rights reserved.
//

#import "MXModel.h"

@implementation MXModel

- (instancetype)initWithDictionary:(NSDictionary *)dic error:(NSError *__autoreleasing *)error {
    self = [super init];
    if (self) {
        if (dic) {
            BOOL success = [self applyValuesFromJSONDic:dic error:error];
            if (!success) {
#if DEBUG
                NSString* errorDescription = (error != NULL && *error != nil) ? [*error description] : @"MXModel validation failed.";
                NSAssert(NO, @"%@", errorDescription);
#endif
                self = nil;
                return self;
            }
        }
    }
    return self;
}

- (instancetype)init {
    return [self initWithDictionary:nil error:NULL];
}

- (BOOL)applyValuesFromJSONDic:(NSDictionary*)dic error:(NSError *__autoreleasing *)error {
    NSDictionary* propertiesToJSONKeyPathsMap = [[self class] JSONKeyPathsByPropertyKey];
    if (!propertiesToJSONKeyPathsMap || !dic) {
        return YES;
    }
    NSNull* nullValue = [NSNull null];
    __block BOOL success = YES;
    [propertiesToJSONKeyPathsMap enumerateKeysAndObjectsUsingBlock:^(NSString* propertyName, NSString* jsonKeyPath, BOOL *stop) {
        id value = [dic valueForKeyPath:jsonKeyPath];
        if (value == nullValue) {
            value = nil;
        }
        if (value) {
            // give subclasses a chance to validate/change values
            NSError* validationError = nil;
            BOOL didValidationSucceed = [self validateValue:&value forKey:propertyName error:&validationError];
            if (!didValidationSucceed) {
                if (error != NULL) {
                    NSMutableDictionary* userInfo = [[NSMutableDictionary alloc] init];
                    userInfo[NSLocalizedDescriptionKey] = [NSString stringWithFormat:@"%@ validation failed for key %@.", NSStringFromClass(self.class), propertyName];
                    if (validationError) {
                        userInfo[NSUnderlyingErrorKey] = validationError;
                    }
                    *error = [NSError errorWithDomain:@"MXModelErrorDomain" code:-1 userInfo:userInfo];
                }
                success = NO;
                *stop = YES;
            } else {
                [self setValue:value forKey:propertyName];
            }
        }
    }];
    return success;
}

+ (NSArray *)fromArray:(NSArray<NSDictionary *> *)jsonArray error:(NSError *__autoreleasing *)error {
    NSMutableArray* models = [[NSMutableArray alloc] initWithCapacity:jsonArray.count];
    for (NSDictionary* dic in jsonArray) {
        id model = [[[self class] alloc] initWithDictionary:dic error:error];
        if (!model) {
            return nil;
        }
        [models addObject:model];
    }
    return models;
}

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return nil;
}

@end
