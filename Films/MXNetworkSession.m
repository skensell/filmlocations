//
//  MXNetworkSession.m
//  Films
//
//  Created by Scott Kensell on 2/25/18.
//  Copyright © 2018 Scott Kensell. All rights reserved.
//

#import "MXNetworkSession.h"

@interface MXNetworkSession()

@property (nonatomic, strong) NSURLSession* session;

@end

@implementation MXNetworkSession

- (instancetype)init {
    self = [super init];
    if (self) {
        _session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:nil delegateQueue:[NSOperationQueue mainQueue]];
    }
    return self;
}

+ (instancetype)instance {
    static id instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once (&onceToken, ^{
        instance = [[[self class] alloc] init];
    });
    return instance;
}

- (void)fetchFilmLocationsCompletion:(MXNetworkSessionDataTaskCompletionBlock)completion {
    NSURL* url = [NSURL URLWithString:@"http://assets.nflxext.com/ffe/siteui/iosui/filmData.json"];
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSURLSessionDataTask* task = [self.session dataTaskWithRequest:request completionHandler:completion];
    [task resume];
}

- (void)fetchArbitraryJSONData:(NSURL *)url completion:(MXNetworkSessionDataTaskCompletionBlock)completion {
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSURLSessionDataTask* task = [self.session dataTaskWithRequest:request completionHandler:completion];
    [task resume];
}

@end
