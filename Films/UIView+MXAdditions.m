//
//  UIView+MXAdditions.m
//  Films
//
//  Created by Scott Kensell on 2/25/18.
//  Copyright © 2018 Scott Kensell. All rights reserved.
//

#import "UIView+MXAdditions.h"

@implementation UIView (MXAdditions)

- (void)mx_addConstraintsForSpanningChildView:(UIView *)child {
    child.translatesAutoresizingMaskIntoConstraints = NO;
    [NSLayoutConstraint activateConstraints:@[
        [self.leftAnchor constraintEqualToAnchor:child.leftAnchor],
        [self.rightAnchor constraintEqualToAnchor:child.rightAnchor],
        [self.topAnchor constraintEqualToAnchor:child.topAnchor],
        [self.bottomAnchor constraintEqualToAnchor:child.bottomAnchor]
    ]];
}

@end
