//
//  MXFilmLocationAnnotation.h
//  Films
//
//  Created by Scott Kensell on 7/3/19.
//  Copyright © 2019 Scott Kensell. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MXFilmLocationAnnotation : NSObject <MKAnnotation>

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;

@end
