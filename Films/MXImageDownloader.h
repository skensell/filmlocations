//
//  MXImageDownloader.h
//  Films
//
//  Created by Scott Kensell on 7/2/19.
//  Copyright © 2019 Scott Kensell. All rights reserved.
//

NS_ASSUME_NONNULL_BEGIN

typedef void(^MXImageDownloaderCompletionBlock)(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error);

@interface MXImageDownloader : NSObject

- (void)downloadImageAtURL:(NSURL*)imageURL completion:(MXImageDownloaderCompletionBlock)completion;
- (void)invalidateAndCancel;

@end

NS_ASSUME_NONNULL_END
