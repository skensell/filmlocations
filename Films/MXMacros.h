//
//  MXMacros.h
//  Films
//
//  Created by Scott Kensell on 2/26/18.
//  Copyright © 2018 Scott Kensell. All rights reserved.
//

#ifndef MXMacros_h
#define MXMacros_h

#define MX_NOT_DESIGNATED_INITIALIZER NSAssert(NO, @"%@ is not the designated initializer for instances of %@.", NSStringFromSelector(_cmd), NSStringFromClass([self class]));
#define MX_ASSERT_ABSTRACT_METHOD NSAssert(NO, @"Abstract method not implemented: %@", NSStringFromSelector(_cmd));

#endif /* MXMacros_h */
