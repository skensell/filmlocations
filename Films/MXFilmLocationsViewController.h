//
//  MXFilmLocationsViewController.h
//  Films
//
//  Created by Scott Kensell on 7/2/19.
//  Copyright © 2019 Scott Kensell. All rights reserved.
//

@class MXNetworkSession;

@interface MXFilmLocationsViewController : UIViewController

- (instancetype)initWithNetworkSession:(MXNetworkSession *)networkSession;

@end
