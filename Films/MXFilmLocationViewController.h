//
//  MXFilmLocationViewController.h
//  Films
//
//  Created by Scott Kensell on 7/2/19.
//  Copyright © 2019 Scott Kensell. All rights reserved.
//

@class MXFilmLocation;
@class MXNetworkSession;

@interface MXFilmLocationViewController : UIViewController

@property (nonatomic, strong, readonly) MXFilmLocation* filmLocation;

- (instancetype)initWithFilmLocation:(MXFilmLocation*)filmLocation networkSession:(MXNetworkSession*)networkSession;

@end
