//
//  MXFilmLocationsViewController.m
//  Films
//
//  Created by Scott Kensell on 7/2/19.
//  Copyright © 2019 Scott Kensell. All rights reserved.
//

#import "MXFilmLocationsViewController.h"

#import "MXFilmLocation.h"
#import "MXFilmLocationViewController.h"
#import "MXNetworkSession.h"

@interface MXFilmLocationsViewController () <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>

// UI
@property (nonatomic, strong) UITableView* tableView;
@property (nonatomic, strong) UISearchBar* searchBar;

// Dependencies
@property (nonatomic, strong) MXNetworkSession* networkSession;

// Internal
@property (nonatomic, strong) NSArray<MXFilmLocation*>* filmLocations;
@property (nonatomic, strong) NSArray<MXFilmLocation*>* filteredFilmLocations;

@end

@implementation MXFilmLocationsViewController

- (instancetype)initWithNetworkSession:(MXNetworkSession *)networkSession {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _networkSession = networkSession;
    }
    return self;
}

- (instancetype)init {
    return [self initWithNetworkSession:[MXNetworkSession instance]];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    return [self init];
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    return [self init];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Film Locations";
    UISearchBar* searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 100, 52.0f)];
    searchBar.searchBarStyle = UISearchBarStyleMinimal;
    searchBar.delegate = self;
    self.searchBar = searchBar;
    
    UITableView* tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    self.tableView = tableView;
    self.tableView.tableHeaderView = self.searchBar;
    [self.view addSubview:self.tableView];
    [self.view mx_addConstraintsForSpanningChildView:self.tableView];
    
    [self fetchAndPresentFilmLocationsData];
}

#pragma mark - Network

- (void)fetchAndPresentFilmLocationsData {
    __weak typeof(self) weakSelf = self;
    [self.networkSession fetchFilmLocationsCompletion:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        // NOTE: I normally use something like @weakify/@strongify from
        // https://github.com/jspahrsummers/libextobjc/blob/master/extobjc/EXTScope.h#L45
        __strong typeof(self) self = weakSelf;
        if (!data) {
            [self showAlertWithTitle:@"Network Error" message:[NSString stringWithFormat:@"The list of films could not be fetched. Here's what we know: %@", [error localizedDescription]]];
            return;
        }
        
        NSError* deserializingError = nil;
        NSArray* filmLocationsArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:&deserializingError];
        if (!filmLocationsArray || ![filmLocationsArray isKindOfClass:[NSArray class]]) {
            [self showAlertWithTitle:@"Data Error" message:[NSString stringWithFormat:@"The list of films could not be parsed because the data is not in the expected format. Here's what we know: %@", [deserializingError localizedDescription]]];
            return;
        }
        
        NSError* instantiationError = nil;
        NSArray* filmLocations = [MXFilmLocation fromArray:filmLocationsArray error:&instantiationError];
        if (!filmLocations) {
            [self showAlertWithTitle:@"Model Error" message:[NSString stringWithFormat:@"The list of films could not be converted to the desired format. Here's what we know: %@", [instantiationError localizedDescription]]];
            return;
        }
        
        self.filmLocations = filmLocations;
        [self filterFilmLocationsWithSearchText:self.searchBar.text];
    }];
}


#pragma mark - UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.filteredFilmLocations.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* reuseIdentifier = @"FilmLocationTableViewCellReuseIdentifier";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    }
    MXFilmLocation* filmLocation = self.filteredFilmLocations[indexPath.row];
    cell.textLabel.text = filmLocation.locationName;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ · %@", filmLocation.title, filmLocation.releaseYear];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    MXFilmLocationViewController* vc = [[MXFilmLocationViewController alloc] initWithFilmLocation:self.filteredFilmLocations[indexPath.row] networkSession:self.networkSession];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if (self.filmLocations.count == 0) {
        return;
    }
    [self filterFilmLocationsWithSearchText:searchText];
}

#pragma mark - Other

- (void)filterFilmLocationsWithSearchText:(NSString*)searchText {
    NSString* text = [[searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] lowercaseString];
    if (text.length == 0) {
        self.filteredFilmLocations = self.filmLocations;
        [self.tableView reloadData];
        return;
    }
    NSMutableArray* filteredFilmLocations = [[NSMutableArray alloc] init];
    [self.filmLocations enumerateObjectsUsingBlock:^(MXFilmLocation * _Nonnull filmLocation, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([filmLocation.locationName.lowercaseString containsString:text]) {
            [filteredFilmLocations addObject:filmLocation];
        }
    }];
    self.filteredFilmLocations = filteredFilmLocations;
    [self.tableView reloadData];
}


#pragma mark - Utilities

- (void)showAlertWithTitle:(NSString*)title message:(NSString*)message {
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}]];
    [self presentViewController:alertController animated:YES completion:nil];
}


@end
