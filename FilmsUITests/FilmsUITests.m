//
//  FilmsUITests.m
//  FilmsUITests
//
//  Created by Scott Kensell on 7/2/19.
//  Copyright © 2019 Scott Kensell. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface FilmsUITests : XCTestCase

@end

@implementation FilmsUITests {
    XCUIApplication* _app;
}

- (void)setUp {
    [super setUp];
    self.continueAfterFailure = NO;
    _app = [[XCUIApplication alloc] init];
    [_app launch];
}

- (void)tearDown {
    _app = nil;
    [super tearDown];
}

- (void)testOpeningEveryFilmLocationDoesNotEverCrash {
    XCUIElement* filmLocationsTable = [_app.tables element];
    NSUInteger totalCells = filmLocationsTable.cells.count;
    for (NSUInteger i = 0; i < totalCells; i++) {
        XCUIElement* cell = [filmLocationsTable.cells elementBoundByIndex:i];
        [cell tap];
        [[_app.navigationBars.buttons firstMatch] tap];
    }
}

@end
